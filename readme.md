Technologies: HTML5, CSS3, JS, Gulp, SCSS, Git, Node.js, npm.

Participants: Valery Trots, Oleh Ivanov.

Tasks:
Oleh Ivanov(task for student №1):
- layout of the site header with the top menu, including the drop-down menu with a small screen expansion;
- layout  section of the People Are Talking About Fork;
- Gulp settings.

Valery Trots(task for student №2):
- layout of the Revolutionary Editor block;
- section layout Here is what you get;
- layout section of the Fork Subscription Pricing section.
- Gulp settings.

Project launch - write "gulp" in terminal.
